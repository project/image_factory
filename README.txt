-- SUMMARY --

The Image factory module can be used to resize images on the fly and 
add infinite captions with custom fonts. It saves pictures in the public 
files path and adds records to the file_managed table.


-- Features --

* You can set width and height to resize area.
   You can upload fonts and manage them.

-- REQUIREMENTS --

* GD library


-- INSTALLATION --

  Install as usual module.Make sure path public://image_factory/ is not used.

-- Usage --

  First use
    Upload ttf font files (in path [admin/image_factory/fonts]).
  Entry
    You can find module entry in media area in configuration page.
  Usage of color picker
    Once you click color field, you will see a popup with color select area.
    You can select color you want by draging white circle on it. 
    While dragging the white circle, the value of color field will change.
  Attention of transparent background
    If you create transparent background, 
    background of thumbnail created by 
    image style function will be black,not transparent.
  Temporary pictures
    When you create pictures using Image factory, 
    it saves temporary files in public://imagefactory/tmp. 
    There's a cron job which automatically removes them.
  How to choose the image you create
    Try the "Visual select file" module
    (http://drupal.org/project/visual_select_file),
    it has a Reference existing function.

-- Module url --

* http://drupal.org/sandbox/jeffstric/1937132


-- CONTACT --

Current maintainers:
* jeffstric (jeffsc.info) - http://drupal.org/user/2507868


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions
