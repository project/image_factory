<?php

/**
 * @file
 * This file process image save operation.
 */

/**
 * Menu callback;  confirm save image.
 */
function image_factory_image_save_confirm($form, $form_state) {
  require drupal_get_path('module', 'image_factory') . '/includes/lib/image_path_save_get.inc';

  drupal_add_js(drupal_get_path('module', 'image_factory') . '/includes/web/js/save_cancel.js');
  $image_src = image_factory_path_ser_to_web(image_factory_get_image_path());

  $form['image'] = array(
    '#type' => 'hidden',
    '#value' => image_factory_web_url_to_ser($image_src),
    '#prefix' => l(t('Back to image factory'), 'admin/image_factory/list') .
    '<img style="display:block;border:2px dotted #aaa;" src="' . $image_src . '"/>',
  );

  $message = t('Are you sure you want to save this image');

  return confirm_form($form, $message, 'admin/image_factory/fonts', '', t('Save'));
}

/**
 * Implements hook_validate().
 */
function image_factory_image_save_confirm_validate($form, $form_state) {
  if (!file_exists($form_state['values']['image'])) {
    form_set_error(t("File @file doesn't exist!", array('@file' => $form_state['values']['image'])));
  }
  return FALSE;
}

/**
 * Implements hook_submit().
 */
function image_factory_image_save_confirm_submit($form, $form_state) {
  $image = $form_state['values']['image'];
  $full_name = explode('/', $image);
  $full_name = array_pop($full_name);

  $image_info = getimagesize($image);
  if ($image_info['mime'] != 'image/png') {
    form_set_error(t('Image type must be png'));
  }

  if (file_exists($image)) {
    $path = image_factory_dir('create');
    $new_file = $path . DIRECTORY_SEPARATOR . $full_name;
    if (!rename($image, $new_file)) {
      form_set_error(t('Move file:@file_old to @file_new faile', array('@file_old' => $image, '@file_new' => $new_file))
      );
    }

    global $user;
    $file = new stdClass();
    $file->uid = $user->uid;
    $file->filename = $full_name;
    $file->uri = image_factory_path_public_to_ser($new_file, TRUE);
    $file->filemime = $image_info['mime'];
    $file->filesize = filesize($new_file);
    $file->status = 1;
    $file->timestamp = time();

    $file_id = file_save($file);
    if ($file_id) {
      file_usage_add($file, 'image_factory', 'image_factory', 1);
      if (!function_exists('image_factory_cookie_delete')) {
        require drupal_get_path('module', 'image_factory') . '/includes/lib/image_path_save_get.inc';
      }
      // Delete src in cookie.
      image_factory_cookie_delete();
      drupal_goto('admin/image_factory/list');
    }
    else {
      form_set_error(t('Image:@image save to database fail', array('@imge' => $image)));
    }
  }
  else {
    form_set_error(t("File @file doesn't exist!", array('@file' => $form_state['values']['image'])));
  }
}
