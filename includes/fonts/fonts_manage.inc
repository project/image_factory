<?php

/**
 * @file
 * this file is used to manage fonts
 */

/**
 * Show fonts mange view.
 * 
 * @return string
 *   The view of fonts manage.
 */
function image_factory_font_list() {
  $output = $a_html
    = '<div class="image_factory_fonts_upload"><ul><li>'
      . l(t('Upload fonts'), 'admin/image_factory/fonts/upload', array('absolute' => TRUE)) . '</li><li>'
      . l(t('Back to image factory'), 'admin/image_factory/list', array('absolute' => TRUE)) .
      '</li></ul></div>';

  $font_pic_path = image_factory_dir('fonts_image_public');
  $fonts = image_factory_get_fonts();
  $rows = array();
  if ($fonts) {
    foreach ($fonts as $value) {
      $rows[] = array(
        $value,
        '<img src="' . file_create_url($font_pic_path . '/' . $value . '.png') . '"/>',
        l(t('remove'), 'admin/image_factory/fonts/del/' . $value, array('absolute' => TRUE)),
      );
    }
    $table_info = array(
      'header' => array(t('Font family'), t('Font example'), t('Operate')),
      'rows' => $rows,
    );
    $output .= theme('table', $table_info);
    if (count($fonts) > 10) {
      $output .= $a_html;
    }
  }
  return $output;
}

/**
 * Form constructor for the fonts upload form.
 * 
 * @return string
 *   The array of form.
 */
function image_factory_fonts_upload_form($form, &$form_state) {
  $form['#method'] = 'post';
  $form['#attributes']['enctype'] = 'multipart/form-data';
  $form['fonts'] = array(
    '#type' => 'file',
    '#title' => t('Upload fonts'),
    '#size' => 48,
    '#description' => t('The fonts you want upload.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Form validation handler for image_factory_fonts_upload_form().
 */
function image_factory_fonts_upload_form_validate($form, &$form_state) {
  if (preg_match('|[^\w\_\-\.]|i', $_FILES['files']['name']['fonts'])) {
    form_set_error('fonts', t('File name should only allow character or "-" or "_" '));
  }
  $_FILES['files']['name']['fonts'] = strtolower($_FILES['files']['name']['fonts']);
}

/**
 * Form submission handler for image_factory_fonts_upload_form().
 */
function image_factory_fonts_upload_form_submit($form, &$form_state) {
  $validators = array(
    'file_validate_extensions' => array('ttf'),
  );
  $destination_path = image_factory_dir('fonts_public');
  $file = file_save_upload('fonts', $validators, $destination_path);
  if ($file) {
    try {
      $file->status = 1;
      file_save($file);
      drupal_set_message(t('Upload fonts success!'));
    }
    catch (Exception $e) {
      drupal_set_message($e->getMessage());
    }
  }
  // Create fonts picture.
  if (image_factory_fonts_image_create()) {
    drupal_set_message(t('Creating fonts picture success'));
  }
  drupal_goto('admin/image_factory/fonts');
}

/**
 * Menu callback;  confirm delete fonts.
 */
function image_factory_fonts_delete_confirm($form, &$form_state, $fonts_name) {
  $fonts_image_src = file_create_url(image_factory_dir('fonts_image_public')) . '/' . $fonts_name . '.png';
  $form['fonts_name'] = array(
    '#type' => 'hidden',
    '#value' => $fonts_name,
    '#prefix' => '<img src="' . $fonts_image_src . '"/>',
  );

  $message = t('Are you sure you want to delete the fonts: %fonts?', array('%fonts' => $fonts_name));
  $caption = '<p>' . t('This action cannot be undone.') . '</p>';

  return confirm_form($form, $message, 'admin/image_factory/fonts', $caption, t('Delete'));
}

/**
 * Process delete fonts.
 */
function image_factory_fonts_delete_confirm_submit($form, &$form_state) {
  if (isset($form_state['values']['fonts_name'])) {
    $font = $form_state['values']['fonts_name'];
    $font_path = image_factory_dir('fonts');
    $font_with_path = $font_path . DIRECTORY_SEPARATOR . $font . '.ttf';
    if (file_exists($font_with_path)) {
      if (unlink($font_with_path)) {
        drupal_set_message(t('Remove font @font success!', array('@font' => $font)));
      }
    }
    else {
      drupal_set_message(t('Remove font @font  fail: no font file', array('@font' => $font)));
    }
    // Delete fonts record in database.
    image_factory_font_del_db($font);
    // Delete fonts thumb pic.
    image_factory_font_del_thumb($font);
    drupal_goto('admin/image_factory/fonts');
  }
  form_set_error('fonts_name', t('Delete fonts fail, check parm input.'));
}

/**
 * Delete fonts info from database(include file_managed and file_usage ).
 * 
 * @param string $font
 *   font's file name(without path)
 */
function image_factory_font_del_db($font) {
  $fonts_filename = $font . '.ttf';
  $fonts_pic_path = image_factory_dir('fonts_public');
  $fonts_filename_path = $fonts_pic_path . '/' . $fonts_filename;
  $result = db_select('file_managed', 'f')->fields('f', array('fid'))
      ->condition('uri', $fonts_filename_path)
      ->execute()
      ->fetchAssoc();

  if ($result && isset($result['fid'])) {
    $file = file_load($result['fid']);
    file_delete($file);
  }
}

/**
 * Delete fonts thumb pic.
 * 
 * @param string $font
 *   font's file name(without path)
 */
function image_factory_font_del_thumb($font) {
  $fonts_pic_path = image_factory_dir('fonts_image');
  $fonts_file_path = $fonts_pic_path . DIRECTORY_SEPARATOR . $font . '.png';
  if (file_exists($fonts_file_path)) {
    unlink($fonts_file_path);
  }
}

/**
 * Create fonts image.
 * 
 * @throws Exception
 */
function image_factory_fonts_image_create() {
  try {
    $fonts = image_factory_get_fonts();
    $fonts_path = image_factory_dir('fonts');
    if (is_array($fonts) && count($fonts)) {
      require drupal_get_path('module', 'image_factory') . '/includes/lib/draw_multi_text.inc';
      $background_img_path = drupal_get_path('module', 'image_factory') . '/includes/web/image/fontShow.png';
      $fonts_pic_path = image_factory_dir('fonts_image');
      foreach ($fonts as $key => $value) {
        // Prepare fonts image info.
        $input = array(
          'N' => 1,
          'B' => $background_img_path,
          'X0' => 0,
          'Y0' => 30,
          'S0' => 30,
          'A0' => 0,
          'C0' => '000000',
          'F0' => $key,
          'H0' => 1,
          'T0' => 'abcdefghijklmn',
        );
        $msg = '';
        $file_name = $fonts_pic_path . '/' . $value . '.png';
        $img = new ImageFactoryDrawMultiText($input);
        $result_img = $img->drawText($file_name, $fonts, $fonts_path, $msg);
        if (!$result_img) {
          throw new Exception($msg);
        }
      }
    }
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
  }
}

/**
 * Remove font thumb which is unnecessary.
 */
function image_factory_font_thumb_clear() {
  $fonts = image_factory_get_fonts();
  $fonts_pic_path = image_factory_dir('fonts_image');
  if (is_dir($fonts_pic_path)) {
    $fso = opendir($fonts_pic_path);
    while ($flist = readdir($fso)) {
      if ($flist == '.' || $flist == '..') {
        continue;
      }
      $file_array = explode('.', $flist);
      if (strtolower(array_pop($file_array)) == 'png') {
        $images[] = implode('.', $file_array);
      }
    }
    closedir($fso);
  }
  $image_del = array_diff($images, $fonts);
  if (count($image_del)) {
    foreach ($image_del as $val) {
      $file_with_path = $fonts_pic_path . DIRECTORY_SEPARATOR . $val . '.png';
      echo $file_with_path . '<br/>';
      if (file_exists($file_with_path)) {
        unlink($file_with_path);
      }
    }
  }
}
