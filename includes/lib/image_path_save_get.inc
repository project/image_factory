<?php

/**
 * @file
 * This file is used to decode or encode image path.
 * So the image path can be transfer in url and decode back.
 */

define('IMAGE_FACTORY_SRC_COOK_NAME', 'image_factory_image_src');

/**
 * Save image path in cookie.
 * 
 * @param string $image_path
 *   The image path need to save.
 */
function image_factory_save_image_path($image_path) {
  user_cookie_save(array(IMAGE_FACTORY_SRC_COOK_NAME => $image_path));
}

/**
 * Get image path from cookie.
 * 
 * @return string
 *   The origin image path.
 */
function image_factory_get_image_path() {
  $imag_src = isset($_COOKIE['Drupal_visitor_' . IMAGE_FACTORY_SRC_COOK_NAME]) ?
      $_COOKIE['Drupal_visitor_' . IMAGE_FACTORY_SRC_COOK_NAME] : FALSE;
  if (!$imag_src) {
    drupal_set_message(t('Image src not exits'), 'error');
    drupal_goto('admin/image_factory/list');
  }
  return $imag_src;
}

/**
 * Delete image path in cookie.
 */
function image_factory_cookie_delete() {
  user_cookie_delete(IMAGE_FACTORY_SRC_COOK_NAME);
}
