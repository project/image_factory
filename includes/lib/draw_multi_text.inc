<?php

/**
 * @file
 * This file is used to draw text to pic
 */

class ImageFactoryDrawMultiText {

  protected $input = array();
  protected $checkParmBasic = array(
    'B' => 'background image',
    'N' => 'num of text',
  );
  protected $checkParmText = array(
    'X' => 'font x postion',
    'Y' => 'font y postion',
    'S' => 'font size',
    'A' => 'font angle',
    'C' => 'font color',
    'F' => 'font family',
    'T' => 'the text to draw',
    'H' => 'the text shadow',
  );
  public $error = FALSE;

  /**
   * Construct class.
   * 
   * @param array $input
   *   Array mix $checkParmBasic and $checkParmText,
   *   with key:'B','N' ,'X0','Y0'...'H0'(,'X1'...'H1')..
   */
  public function __construct($input) {
    $this->input = &$input;
    $this->checkInput($input);
  }

  /**
   * Check class input parm.
   */
  public function checkInput() {
    try {
      // Check basic param.
      $this->checkBasic();
      // Check detial param.
      $this->checkDetail();
    }
    catch (Exception $e) {
      $this->error = $e->getMessage();
    }
  }

  /**
   * Check basic input info.
   * 
   * @throws Exception
   */
  protected function checkBasic() {
    foreach ($this->checkParmBasic as $k => $v) {
      if ($this->checkSingle($k) === FALSE) {
        throw new Exception('wrong param: ' . $this->checkParmBasic[$k] . '; key: ' . $k);
      }
    }
  }

  /**
   * Check detail input info.
   */
  protected function checkDetail() {
    $text_num = $this->input['N'];
    if (count($text_num)) {
      for ($i = 0; $i < $text_num; $i++) {
        // Prepare checkParm to check the text param.
        foreach ($this->checkParmText as $key => $value) {
          if ($this->checkSingle($key . $i) === FALSE) {
            throw new Exception('wrong param: ' . $this->checkParmText[$key] . '; key: ' . $key . $i);
          }
        }
      }
    }
  }

  /**
   * Check single input info.
   * 
   * @param string $key
   *   Key of post
   * 
   * @return bool
   *   Result of check
   */
  protected function checkSingle($key) {
    return isset($this->input[$key]) ? $this->input[$key] : FALSE;
  }

  /**
   * Get color info.
   * 
   * @param string $color
   *   Str of color(with out '#')
   * 
   * @return array
   *   Array of color.
   * @throws Exception
   */
  protected function imageFactoryTransferColor($color) {
    try {
      if (strlen($color) == 3) {
        $c1 = substr($color, 0, 1);
        $c2 = substr($color, 1, 1);
        $c3 = substr($color, 2, 1);
        $color = $c1 . $c1 . $c2 . $c2 . $c3 . $c3;
      }
      if (strlen($color) == 6) {
        $red = hexdec(substr($color, 0, 2));
        $green = hexdec(substr($color, 2, 2));
        $blue = hexdec(substr($color, 4, 2));
        return array($red, $green, $blue);
      }
      else {
        throw new Exception('color param length illegal');
      }
    }
    catch (Exception $e) {
      throw new Exception('color param illegal; C: ' . $color);
    }
  }

  /**
   * Draw text to picture.
   * 
   * @param string $output_pic
   *   Where to store image created.
   * @param string $fonts
   *   The name of font.
   * @param string $fonts_path
   *   The path of font.
   * @param string $msg
   *   The message of function.
   * 
   * @return bool
   *   Success or fail.
   * @throws Exception
   */
  public function drawText($output_pic, $fonts, $fonts_path, &$msg = '') {
    try {
      if ($this->error) {
        throw new Exception($this->error);
      }
      $input = $this->input;
      // Check image exist.
      $file_name = $input['B'];
      if (!file_exists($file_name)) {
        throw new Exception("Background image doesn't exist");
      }
      // Draw background now.
      list($width, $height, $type) = getimagesize($file_name);
      switch ($type) {
        case 1:
          $im = imagecreatefromgif($file_name);
          break;

        case 2:
          $im = imagecreatefromjpeg($file_name);
          break;

        case 3:
          $im = imagecreatefrompng($file_name);
          break;

        default:
          throw new Exception('illegal image type ');
      }
      // Create some colors.
      $grey = imagecolorallocate($im, 128, 128, 128);
      // Get the number of texts.
      $text_num = $input['N'];
      if (count($text_num)) {
        for ($i = 0; $i < $text_num; $i++) {
          // Check text font family.
          if ($input['F' . $i] > count($fonts) - 1) {
            throw new Exception('illegal font param: ' . $input['F' . $i] . '; key: ' . 'F' . $i);
          }
          // Get font color.
          $font_color_array = $this->imageFactoryTransferColor($input['C' . $i]);
          $font_color = imagecolorallocate($im, $font_color_array[0], $font_color_array[1], $font_color_array[2]);
          // Replace path by your own font path.
          $font = $fonts_path . DIRECTORY_SEPARATOR . $fonts[$input['F' . $i]] . '.ttf';
          if (file_exists($font)) {
            // Add some shadow to the text.
            if ($input['H' . $i]) {
              imagettftext(
                  $im, $input['S' . $i], $input['A' . $i], $input['X' . $i] + 1, $input['Y' . $i] + 1, $grey, $font, $input['T' . $i]);
            }
            // Add the text.
            imagettftext(
                $im, $input['S' . $i], $input['A' . $i], $input['X' . $i], $input['Y' . $i], $font_color, $font, $input['T' . $i]);
          }
          else {
            throw new Exception('font dosen\'t exist!');
          }
        }
      }
      $result = imagepng($im, $output_pic);
      imagedestroy($im);
      return $result;
    }
    catch (Exception $e) {
      $msg = $e->getMessage();
      return FALSE;
    }
  }

}
