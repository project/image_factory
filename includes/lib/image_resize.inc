<?php

/**
 * @file
 * This file is used to resize
 */

class ImageFactoryResizeImage {

  protected static $c1 = 255;
  protected static $c2 = 255;
  protected static $c3 = 255;

  /**
   * Resize image.
   * 
   * @param string $image_form
   *   Source image  link resource.
   * @param string $image_to_path
   *   Destination  path.
   * @param int $width_to
   *   Destination width. 
   * @param int $height_to
   *   Destination height. 
   * @param int $top
   *   X-coordinate of source point. 
   * @param int $left
   *   Y-coordinate of source point. 
   * @param string $error
   *   The message if error.
   * 
   * @return string|boolean
   *   Resize success or fail.
   * 
   * @throws Exception
   */
  public static function reseizeImage($image_form, $image_to_path, $width_to, $height_to, $top, $left, &$error = '') {
    if (!preg_match('|[\\\/]|', substr($image_to_path, -1, 0))) {
      $image_to_path .= '/';
    }
    $file_name_src = explode('.', $image_form);
    $ext_name = array_pop($file_name_src);
    $file_output = $image_to_path . time() . '.' . $ext_name;
    if (file_exists($image_form)) {
      list($width, $height, $type) = getimagesize($image_form);
      $im_form = FALSE;
      switch ($type) {
        case 1:
          $im_form = @imagecreatefromgif($image_form);
          break;

        case 2:
          $im_form = @imagecreatefromjpeg($image_form);
          break;

        case 3:
          $im_form = @imagecreatefrompng($image_form);
          break;

        default:
          throw new Exception('wrong type image');
      }
      if ($im_form) {
        $im_to = imagecreatetruecolor($width_to, $height_to);
        $color = imagecolorallocate($im_to, self::$c1, self::$c2, self::$c3);
        imagefill($im_to, 0, 0, $color);
        $copy_result = imagecopyresampled($im_to, $im_form, 0, 0, $left, $top, $width_to, $height_to, $width_to, $height_to);
        if ($copy_result) {
          $return = FALSE;
          switch ($type) {
            case 1:
              $return = imagegif($im_to, $file_output);
              break;

            case 2:
              $return = imagejpeg($im_to, $file_output);
              break;

            case 3:
              $return = imagepng($im_to, $file_output);
              break;

            default:
              throw new Exception('wrong type image');
          }
          imagedestroy($im_form);
          imagedestroy($im_to);
          if ($return) {
            return $file_output;
          }
          else {
            $error = 'create image fail';
          }
        }
        else {
          $error = 'copy image fail';
        }
      }
      else {
        $error = 'create im image fail';
      }
    }
    else {
      $error = 'Target file dosen\'t exist';
    }
    return FALSE;
  }

  /**
   * Get radio of To/From.
   * 
   * @param float $width_from
   *   The width of origin. 
   * @param float $height_from
   *   The height of origin.
   * @param float $width_to
   *   The width of destination.
   * @param float $height_to
   *   The height of destination.
   * @param string $which_radio
   *   'w' means resize by width.
   * 
   * @return float
   *   The Right radio.
   */
  protected function getRadio($width_from, $height_from, $width_to, $height_to, &$which_radio = '') {
    $radio_width = $width_to / $width_from;
    $radio_heigt = $height_to / $height_from;
    $radio = FALSE;
    $which_radio = FALSE;
    if (($height_from > $height_to) || ($width_from > $width_to)) {
      if ($radio_width < $radio_heigt) {
        $radio = $radio_width;
        $which_radio = 'w';
      }
      else {
        $radio = $radio_heigt;
        $which_radio = 'h';
      }
    }
    return $radio;
  }

}
