<?php

/**
 * @file
 * this file is used to valid and submit for
 */

/**
 * Form constructor for the image upload.
 * 
 * @return array
 *   The array of form.
 */
function image_factory_image_upload_form($form, &$form_state) {
  $form['#method'] = 'post';
  $form['#attributes']['enctype'] = 'multipart/form-data';
  $form['picture'] = array(
    '#type' => 'file',
    '#title' => t('Upload picture'),
    '#size' => 48,
    '#description' => t('The picture you want upload.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Form validation handler for image_factory_image_upload_form().
 * 
 * @see image_factory_image_upload_form_submit()
 */
function image_factory_image_upload_form_validate($form, &$form_state) {
  if (preg_match('|[^\w\_\-\.]|i', $_FILES['files']['name']['picture'])) {
    form_set_error('picture', t('File name should only allow character , "-" and "_"'));
  }
}

/**
 * Form submission handler for image_factory_image_upload_form().
 * 
 * @see image_factory_image_upload_form_validate()
 */
function image_factory_image_upload_form_submit($form, &$form_state) {
  $validators = array(
    'file_validate_extensions' => array('jpg png gif'),
  );
  $destination_path = image_factory_dir('upload');

  $file = file_save_upload('picture', $validators, $destination_path);
  if ($file) {
    $file->status = 1;
    try {
      file_save($file);
      // Add file usage.
      file_usage_add($file, 'image_factory', 'image_factory', 1);
      drupal_set_message(t('Upload file success!'));
    }
    catch (Exception $e) {
      drupal_set_message($e->getMessage());
    }
  }
  drupal_goto('admin/image_factory/list');
}

/**
 * Form constructor for the image rename form.
 * 
 * @param int $fid
 *   The file id in file_managed.
 * 
 * @return array
 *   The array of form.
 */
function image_factory_image_rename_form($form, &$form_state, $fid) {
  $file = file_load($fid);
  if ($file) {
    $file_name = explode('.', $file->filename);
    $form['#method'] = 'post';
    $form['picture_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Picture name'),
      '#value' => array_shift($file_name),
      '#description' => t('Please input the name of picture,without filename extension.'),
      '#required' => TRUE,
      '#suffix' => '<div class="image_factory_notice">' .
      t("Notice: update action only change filename in database 
      and won't affect the file in server.") . '</div>',
    );
    // Check fid. /admin/image_factory/rename/:fid.
    if ($fid) {
      $form['fid'] = array(
        '#type' => 'hidden',
        '#value' => $fid,
      );
    }
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );
    return $form;
  }
  else {
    drupal_set_message(t("The file fid=:fid doesn't exist!", array(':fid' => $fid)), 'error');
    drupal_goto('admin/image_factory/list');
  }
}

/**
 * Form validation handler for image_factory_image_rename_form().
 */
function image_factory_image_rename_form_validate($form, &$form_state) {
  if (!$form_state['values']['picture_name']) {
    form_set_error('picture_name', t('Picture name must be enter!'));
  }
  if (preg_match('%[^\w\-_]%', $form_state['values']['picture_name'])) {
    form_set_error('picture_name', t('File name should only allow num, character , "-" and "_"'));
  }
}

/**
 * Form submission handler for image_factory_image_rename_form().
 */
function image_factory_image_rename_form_submit($form, &$form_state) {
  if (isset($form_state['values']['fid'])) {
    $fid = is_numeric($form_state['values']['fid']) ? $form_state['values']['fid'] : 0;
    if ($fid && isset($form_state['values']['picture_name'])) {
      $file = file_load($fid);
      $extent_name = explode('/', $file->filemime);
      $extent_name = $extent_name[1];
      if ($extent_name == 'jpeg') {
        $extent_name = 'jpg';
      }
      $file->filename = $form_state['values']['picture_name'] . '.' . $extent_name;
      file_save($file);
      drupal_set_message(t('Update image name success!'));
      drupal_goto('admin/image_factory/list');
    }
  }
  form_set_error('fid', t('param fid or filename is illegal.'));
}

/**
 * Menu callback;  confirm delete a single image in file_managed.
 */
function image_factory_image_delete_confirm($form, &$form_state, $fid) {
  $file = file_load($fid);
  $form['name'] = array(
    '#type' => 'value',
    '#value' => $file->filename,
    '#prefix' => '<img src=' . image_style_url('thumbnail', $file->uri) . 'alt="' . $file->filename . '"/>',
  );
  $form['fid'] = array('#type' => 'hidden', '#value' => $file->fid);

  $message = t('Are you sure you want to delete the image: %image?', array('%image' => $file->filename));
  $caption = '<p>' . t('This action cannot be undone.') . '</p>';

  return confirm_form($form, $message, 'admin/image_factory/list', $caption, t('Delete'));
}

/**
 * Process delete a single image in file_managed.
 */
function image_factory_image_delete_confirm_submit($form, &$form_state) {
  if (isset($form_state['values']['fid'])) {
    $file = file_load($form_state['values']['fid']);
    if ($file) {
      // Delete recorde of file usage.
      file_usage_delete($file, 'image_factory', 'image_factory', 1);
      $result = file_delete($file);
      if ($result === TRUE) {
        drupal_set_message(t('Delete image: :filename success', array(':filename' => $file->filename)));
      }
      else {
        if (is_array($result)) {
          drupal_set_message(t("Delete image : :filename  fail, it's used by others", array(':filename' => $file->filename)));
        }
      }
      drupal_goto('admin/image_factory/list');
    }
  }
  form_set_error('fid', t('Delete image fail, check parm input.'));
}
