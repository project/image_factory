<?php

/**
 * @file
 * This file is store the resize form.
 */

/**
 * Form constructor for the image resize form.
 * 
 * @return array
 *   The array of form.
 */
function image_factory_image_resize($form, &$form_state, $id) {
  if ($id && is_numeric($id)) {
    $row = file_load($id);
    if ($row) {
      $image_src = file_create_url($row->uri);
      drupal_add_css(drupal_get_path('module', 'image_factory') . '/includes/web/css/resize.css');
      drupal_add_js(drupal_get_path('module', 'image_factory') . '/includes/web/js/image_drage_resize.js');
      $js = <<<EOE
    jQuery(document).ready(function() {
      o = new drag_js(jQuery, '$image_src');
      o.init();
      jQuery('#image_factory_resize_submit').click(function() {
        var r = o.result(); 
        jQuery('#resize_info_top input').val(r.top);
        jQuery('#resize_info_left input').val(r.left);
        jQuery('#resize_info_width input').val(r.width);
        jQuery('#resize_info_height input').val(r.height);
        jQuery('#resize_info_src input').val(r.src);
      });
    });
EOE;
      drupal_add_js($js, 'inline');
      $l = '<ul><li>' . l(t('Go back to image factory'), 'admin/image_factory/list') . '</li><li>' .
          l(t('Use transparent background'), 'admin/image_factory/transparent') . '</li></ul>';
      $form['image_resize_form'] = array(
        '#title' => t("Image resize form"),
        '#type' => 'fieldset',
        '#description' => t('Please click resize button to continue'),
        '#prefix' => $l . theme('image_factory'),
      );
      $form['image_resize_form']['resize_info_top'] = array(
        '#prefix' => '<div id="resize_info_top">',
        '#type' => 'hidden',
        '#suffix' => '</div>',
      );
      $form['image_resize_form']['resize_info_left'] = array(
        '#prefix' => '<div id="resize_info_left">',
        '#type' => 'hidden',
        '#suffix' => '</div>',
      );
      $form['image_resize_form']['resize_info_width'] = array(
        '#prefix' => '<div id="resize_info_width">',
        '#type' => 'hidden',
        '#suffix' => '</div>',
      );
      $form['image_resize_form']['resize_info_height'] = array(
        '#prefix' => '<div id="resize_info_height">',
        '#type' => 'hidden',
        '#suffix' => '</div>',
      );
      $form['image_resize_form']['resize_info_src'] = array(
        '#prefix' => '<div id="resize_info_src">',
        '#type' => 'hidden',
        '#suffix' => '</div>',
      );
      $form['image_resize_form']['submit'] = array(
        '#prefix' => '<div id="image_factory_resize_submit">',
        '#suffix' => '</div>',
        '#value' => t('Submit'),
        '#type' => 'submit',
      );
      return $form;
    }
  }
  drupal_set_message(t("Image doesn't exist"), 'error');
  drupal_goto('admin/image_factory/list');
}

/**
 * Form validation handler for image_factory_image_resize().
 *
 * @see image_factory_image_resize_submit()
 */
function image_factory_image_resize_validate($form, &$form_state) {
  $nums_check = array(
    'resize_info_top', 'resize_info_left',
    'resize_info_width', 'resize_info_height',
  );
  foreach ($nums_check as $value) {
    if ($form_state['values'][$value] < 0) {
      form_set_error($value, t('@value must be greater than zero.', array('@value' => $value)));
    }
  }
  if (!$form_state['values']['resize_info_src']) {
    form_set_error('resize_info_src', t('Image src illegal.'));
  }
}

/**
 * Form submission handler for image_factory_image_resize().
 * 
 * @see image_factory_image_resize_validate()
 */
function image_factory_image_resize_submit($form, &$form_state) {
  require drupal_get_path('module', 'image_factory') . '/includes/lib/image_resize.inc';
  $resize = new ImageFactoryResizeImage();
  $error = '';
  $result = $resize->reseizeImage(
      image_factory_web_url_to_ser($form_state['values']['resize_info_src']), image_factory_dir('tmp'), $form_state['values']['resize_info_width'], $form_state['values']['resize_info_height'], $form_state['values']['resize_info_top'], $form_state['values']['resize_info_left'], $error);
  if (!$result) {
    form_set_error('resize_info', $error);
  }
  else {
    require drupal_get_path('module', 'image_factory') . '/includes/lib/image_path_save_get.inc';
    image_factory_save_image_path($result);
    drupal_goto('admin/image_factory/edit');
  }
}
