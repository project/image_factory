<?php

/**
 * @file
 * this file is used to create transparent background
 */

/**
 * Form constructor for the set transfer background form.
 * 
 * @return array
 *   The array of form.
 */
function image_factory_bg_transparent($form, &$form_state) {
  $form['image_transparent_form'] = array(
    '#title' => t("Image tansparent background form"),
    '#type' => 'fieldset',
    '#description' => t('Please click resize button to continue'),
    '#prefix' => l(t('Go back to image factory'), 'admin/image_factory/list'),
  );
  $form['image_transparent_form']['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
  );
  $form['image_transparent_form']['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
  );
  $form['image_transparent_form']['submit'] = array(
    '#value' => t('Submit'),
    '#type' => 'submit',
  );
  return $form;
}

/**
 * Form validation handler for image_factory_bg_transparent().
 */
function image_factory_bg_transparent_validate($form, &$form_state) {
  if ($form_state['values']['width'] < 0) {
    form_set_error('image_transparent_form', t('Width must greater than zero.'));
  }
  if ($form_state['values']['height'] < 0) {
    form_set_error('image_transparent_form', t('Height must greater than zero.'));
  }
}

/**
 * Form submission handler for image_factory_bg_transparent().
 */
function image_factory_bg_transparent_submit($form, &$form_state) {

  $full_name = image_factory_dir('tmp') . DIRECTORY_SEPARATOR . time() . '.png';
  if (image_factory_create_transfer_img($full_name, $form_state['values']['width'], $form_state['values']['height']
      )) {
    require drupal_get_path('module', 'image_factory') . '/includes/lib/image_path_save_get.inc';
    image_factory_save_image_path($full_name);
    drupal_goto('admin/image_factory/edit/');
  }
  else {
    form_set_error(NULL, t('Create transparent background fail.'));
  }
}

/**
 * Create transferparent image.
 * 
 * @param string $img_path
 *   The image store path(with file name).
 * @param int $width
 *   The width.
 * @param int $height
 *   The height.
 * 
 * @return bool
 *   Create success or not.
 */
function image_factory_create_transfer_img($img_path, $width, $height) {
  $im = imagecreatetruecolor($width, $height);

  $result = FALSE;
  if ($im) {
    // Set background.
    $background = imagecolorallocate($im, 0, 0, 0);
    imagecolortransparent($im, $background);
    imagefill($im, 0, 0, $background);
    $result = imagepng($im, $img_path);
    imagedestroy($im);
  }

  return $result;
}
