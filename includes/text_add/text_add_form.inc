<?php

/**
 * @file
 * This file is store the resize form.
 */

/**
 * Return add text form.
 */
function image_factory_image_edit($form, &$form_state) {

  if (!function_exists('image_factory_decode_image_path')) {
    require drupal_get_path('module', 'image_factory') . '/includes/lib/image_path_save_get.inc';
  }
  $image_link = image_factory_get_image_path();

  if (file_exists($image_link)) {
    return image_factory_image_edit_form($form, $form_state, $image_link);
  }
  else {
    drupal_set_message(t("Image :@image doesn't exist", array('@image' => $image_link)), 'error');
    drupal_goto('admin/image_factory/list');
  }
}

/**
 * Form constructor for the add text to image form.
 * 
 * @param array $form
 *   Drupal form.
 * @param array $form_state
 *   Form state.
 * @param stirng $image_link
 *   The background image path.
 * 
 * @return array
 *   The image form.
 */
function image_factory_image_edit_form($form, &$form_state, $image_link) {
  drupal_add_css(drupal_get_path('module', 'image_factory') . '/includes/web/css/edit.css');
  drupal_add_js(drupal_get_path('module', 'image_factory') . '/includes/web/js/text_add.js');

  $fonts_image = image_factory_get_fonts_images();
  drupal_add_js('var fonts_image = ' . json_encode($fonts_image), 'inline');

  $fonts = image_factory_get_fonts();
  if (!$fonts) {
    drupal_set_message('Please upload font file');
  }
  $fonts_image_src = (count($fonts)) ?
      file_create_url(image_factory_dir('fonts_image_public') . '/' . $fonts[0] . '.png') : '';

  $image_bg = image_factory_path_ser_to_web($image_link);
  $text_nums = !empty($form_state['values']['text_nums']) ? $form_state['values']['text_nums'] : 1;

  $form['#prefix'] = '<div style="margin-bottom:10px">' .
      l(t('Back to image factory'), 'admin/image_factory/list', array('absolute' => TRUE)) . '</div>';
  $form['text_nums'] = array(
    '#type' => 'select',
    '#title' => t('Number of text'),
    '#options' => drupal_map_assoc(range(1, 7)),
    '#default_value' => $text_nums,
    '#ajax' => array(
      'callback' => 'image_factory_image_edit_form_callback',
      'wrapper' => 'text_add_area',
    ),
  );
  $form['background'] = array(
    '#type' => 'hidden',
    '#value' => $image_bg,
  );
  $form['text_add_area'] = array(
    '#type' => 'fieldset',
    '#prefix' => '<div id="text_add_area"><div id="image_factory_origin"><img src="' .
    $image_bg . '"/></div>',
    '#suffix' => '</div>',
  );

  for ($i = 0; $i < $text_nums; $i++) {
    $form['text_add_area']['area' . $i] = array(
      '#type' => 'fieldset',
    );
    $form['text_add_area']['area' . $i]['content' . $i] = array(
      '#type' => 'textfield',
      '#title' => t('Content'),
      '#required' => TRUE,
      '#description' => t("Type the message you'd like display"),
    );
    $form['text_add_area']['area' . $i]['color' . $i] = array(
      '#type' => 'jquery_colorpicker',
      '#default_value' => '945494',
    );
    $form['text_add_area']['area' . $i]['size' . $i] = array(
      '#type' => 'select_or_other',
      '#options' => drupal_map_assoc(range(8, 72, 8)),
      '#default_value' => array(8),
      '#title' => t('Size'),
      '#other' => t('Other (please type a value)'),
      '#required' => TRUE,
      '#description' => t('Unit:') . image_factory_get_unit(),
    );
    $form['text_add_area']['area' . $i]['angle' . $i] = array(
      '#type' => 'select_or_other',
      '#default_value' => array(0),
      '#options' => drupal_map_assoc(range(0, 330, 30)),
      '#other' => t('Other (please type a value)'),
      '#title' => t('Angle'),
      '#required' => TRUE,
    );
    $form['text_add_area']['area' . $i]['X' . $i] = array(
      '#type' => 'textfield',
      '#title' => t('X'),
      '#required' => TRUE,
    );
    $form['text_add_area']['area' . $i]['Y' . $i] = array(
      '#type' => 'textfield',
      '#title' => t('Y'),
      '#required' => TRUE,
      '#description' => t("The coordinates given by x and y will define the 
        basepoint of the first character (roughly the lower-left corner of the character)."),
    );
    $form['text_add_area']['area' . $i]['shadow' . $i] = array(
      '#type' => 'select',
      '#options' => array(
        0 => t('No'), 1 => t('Yes'),
      ),
      '#title' => t('Shadow'),
    );
    $form['text_add_area']['area' . $i]['font' . $i] = array(
      '#type' => 'select',
      '#options' => $fonts,
      '#title' => t('Font family'),
      '#prefix' => '<div class="font">',
      '#suffix' => '<img src="' . $fonts_image_src . '"></div>',
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add text to image'),
    );
  }

  return $form;
}

/**
 * Implements hook_callbaxk().
 */
function image_factory_image_edit_form_callback($form, $form_state) {
  return $form['text_add_area'];
}

/**
 * Form validation handler for image_factory_image_edit().
 */
function image_factory_image_edit_validate($form, $form_state) {

  $font_family = image_factory_get_fonts();
  $num = $form_state['values']['text_nums'];
  $need_checks = array(
    'content', 'color', 'size',
    'X', 'Y', 'angle', 'shadow', 'font',
  );
  for ($i = 0; $i < $num; $i++) {

    foreach ($need_checks as $c) {
      if ($form_state['values'][$c . $i] === '') {
        form_set_error($c . $i, t('@t must input.', array('@t' => $c)));
      }
    }

    if (!$form_state['values']['content' . $i]) {
      form_set_error('content' . $i, t('Content must be input'));
    }
    if (!preg_match('|^(?:[0-9a-fA-F]{3}){1,2}$|', $form_state['values']['color' . $i])) {
      form_set_error('color' . $i, t('Please use three or six letters or numbers to describe your color.'));
    }
    if ($form_state['values']['size' . $i] <= 0) {
      form_set_error('size' . $i, t('Text size must greater than zero.'));
    }
    if (preg_match('|\D|', $form_state['values']['X' . $i]) || intval($form_state['values']['X' . $i]) < 0) {
      form_set_error('X' . $i, t('X must be integer and greater than zero.'));
    }
    if (preg_match('|\D|', $form_state['values']['Y' . $i]) || intval($form_state['values']['Y' . $i]) < 0) {
      form_set_error('Y' . $i, t('Y must be integer and greater than zero.'));
    }
    if ($form_state['values']['angle' . $i] < 0 || $form_state['values']['angle' . $i] > 360) {
      form_set_error('angle' . $i, t('Angle input illegal!'));
    }
    if (!in_array($form_state['values']['shadow' . $i], array(0, 1))) {
      form_set_error('shadow' . $i, t('Shadow input illegal!'));
    }
    if (!in_array($form_state['values']['font' . $i], array_keys($font_family))) {
      form_set_error('font' . $i, t('Font family input illegal!'));
    }
  }
}

/**
 * Form submission handler for image_factory_image_edit().
 */
function image_factory_image_edit_submit($form, $form_state) {
  if (!class_exists('')) {
    require drupal_get_path('module', 'image_factory') . '/includes/lib/draw_multi_text.inc';
  }
  $input = array(
    'B' => image_factory_web_url_to_ser($form_state['values']['background']),
    'N' => $form_state['values']['text_nums'],
  );
  $input_map = array(
    'X' => 'X',
    'Y' => 'Y',
    'S' => 'size',
    'A' => 'angle',
    'C' => 'color',
    'F' => 'font',
    'T' => 'content',
    'H' => 'shadow',
  );
  for ($i = 0; $i < $input['N']; $i++) {
    foreach (array_keys($input_map) as $k) {
      $input[$k . $i] = $form_state['values'][$input_map[$k] . $i];
    }
  }
  $msg = '';
  $output_pic = image_factory_dir('tmp') . '/' . time() . '.png';
  $fonts = image_factory_get_fonts();
  $fonts_path = image_factory_dir('fonts');

  $draw_texts = new ImageFactoryDrawMultiText($input);
  if (!$draw_texts->drawText($output_pic, $fonts, $fonts_path, $msg)) {
    form_set_error('text_add_area', $msg);
    return FALSE;
  }
  else {
    require drupal_get_path('module', 'image_factory') . '/includes/lib/image_path_save_get.inc';
    image_factory_save_image_path($output_pic);
    drupal_goto('admin/image_factory/save');
    return TRUE;
  }
}

/**
 * Get GD libraris unit.
 * 
 * @staticvar string $unit_of_font_size.
 *    The unit of font size.
 * 
 * @return string
 *   The unit of font size in edit form.
 */
function image_factory_get_unit() {
  static $unit_of_font_size = FALSE;
  if (!$unit_of_font_size) {
    $gdinfo = gd_info();
    $gdinfo = explode('.', $gdinfo['GD Version']);
    $unit_of_font_size = ($gdinfo == 1) ? 'pixel' : 'point';
  }
  return $unit_of_font_size;
}

/**
 * Return fonts image web src.
 * 
 * @return array
 *   The web src of fonts image.
 */
function image_factory_get_fonts_images() {
  $fonts = image_factory_get_fonts();
  $font_pic_path = image_factory_dir('fonts_image_public');
  $font_pic_web = array();
  foreach ($fonts as $f) {
    $font_pic_web[] = file_create_url($font_pic_path . '/' . $f . '.png');
  }
  return $font_pic_web;
}
