(function($) {
  $(document).ready(function() {
    $('a#edit-cancel').click(function() {
      window.history.back();
      return false;
    });
  });
})(jQuery);
