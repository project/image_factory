(function($) {
  $(document).ready(function() {
    if (typeof(fonts_image) === 'object' && fonts_image.length) {
      $('body').delegate('.font select', 'change', function() {
        var fonts_image_src = fonts_image[$(this).val()];
        $(this).parents('.font').find('img').attr('src', fonts_image_src);
      });
    }
  });
})(jQuery);
