var drage_js_base = '#drage_image_contianer';
function drag_js($, img_src) {
  this.src = img_src;
  this.container = $(drage_js_base);
  this.opacity = $('#drage_image_opacity');
  this.select = $('#drage_image_select');
  this.select_border_w = this.select.css('border-width');
  this.resize = $('#drage_image_resize');
  this.fieldset = $('#drage_image_fieldset');
  this.field_width = $('#drage_image_width');
  this.field_height = $('#drage_image_height');
  this.resize_size = parseInt(this.resize.css('width'), 10);
  this.resize_active = false;
  this.container.data('drag_js', this);
  this.select_border_w = (this.select_border_w) ? this.select_border_w : this.select.css('border-left-width');
  this.select_border_w = parseInt(this.select_border_w);
}
drag_js.prototype = {
  'init': function() {
// Check image contianer id.
    if (typeof(drage_js_base) === 'undefined') {
      return false;
    }
// Get image height and width;
    img = new Image();
    // Set background;
    img.src = this.src;
    img.onload = function() {
      var dj = jQuery(drage_js_base).data('drag_js');
      dj.select.add(dj.opacity)
              .css('background', 'url("' + this.src + '") no-repeat')
              .eq(0).add(dj.container).css('height', img.height).css('width', img.width);
    };
    this.drage(this.resize, this.fun_resize_valid, this.fun_resize_move, this);
    this.drage(this.select, this.fun_select_valid, this.fun_select_move, this, this.fun_select_callback);
    this.bind_set_wh();
    // Fill width and height to field
    this.fill_field();
  },
  'drage': function(obj, get_valid, process, th) {
    obj.unbind('mousedown');
    obj.bind('mousedown', function(event) {
// If the obj is resize, we can ignore select operate.
      if (obj === th.resize) {
        th.resize_active = true;
      } else {
        if (th.resize_active) {
          return false;
        }
      }
// Store the deviation between mouse and dom position.
      var v = [
        event.clientX - th.get_int_css(obj, 'left'),
        event.clientY - th.get_int_css(obj, 'top')
      ];
      var valid_area = get_valid.call(obj, th);
      // Bind mousemove.
      jQuery('body').mousemove(function(event) {
        process.call(obj, event, v, valid_area, th);
      });
      // Bind mouse up.
      jQuery('body').bind('mouseup', function(event) {
        if (obj === th.resize) {
          th.resize_active = false;
        }
// Unbind mouse move.
        jQuery('body').unbind('mousemove');
        if (arguments[4]) {
          arguments[4](event, obj);
        }
      });
    });
  },
  'fun_resize_valid': function(th) {
    return {
      'w0': [0,
        th.container.width() - parseInt(th.select.css('left'), 10) - th.resize_size - 2 * th.select_border_w],
      'h0': [0,
        th.container.height() - parseInt(th.select.css('top'), 10) - th.resize_size - 2 * th.select_border_w]
    };
  },
  'fun_resize_move': function(event, deviation, v, th) {
    var left = th.valid_num(event.clientX - deviation[0], v['w0'][0], v['w0'][1]);
    var top = th.valid_num(event.clientY - deviation[1], v['h0'][0], v['h0'][1]);
    var w = left + th.resize_size;
    var h = top + th.resize_size;
    this.css('left', left + 'px').css('top', top + 'px').
            parent().width(w).height(h);
    // Fill width and height to field
    th.fill_field();
  },
  'fun_select_valid': function(th) {
    return {
      'w1': [0,
        th.container.width() - th.select.width() - 2 * th.select_border_w],
      'h1': [0,
        th.container.height() - th.select.height() - 2 * th.select_border_w]
    };
  },
  'fun_select_move': function(event, deviation, v, th) {
    var left = th.valid_num(event.clientX - deviation[0], v['w1'][0], v['w1'][1]);
    var top = th.valid_num(event.clientY - deviation[1], v['h1'][0], v['h1'][1]);
    this.css('left', left + 'px').css('top', top + 'px').
            css('background-position', '-' + (th.select_border_w + left) + 'px -' + (th.select_border_w + top) + 'px');
  },
  'fun_select_callback': function(event, obj) {
    var dj = jQuery(drage_js_base).data('drag_js');
    this.resize.unbind('mousedown');
    this.drage(this.resize, this.fun_resize_valid, this.fun_resize_move, di);
  },
  'valid_num': function(num, min, max) {
    num = parseInt(num, 10);
    num = (num) ? num : 0;
    if (num < min) {
      return min;
    } else {
      if (num < max) {
        return num;
      } else {
        return max;
      }
    }
  },
  'get_int_css': function(obj, lr) {
    var r = parseInt(obj.css(lr), 10);
    return r ? r : 0;
  },
  'get_result': function() {
    return {
      'top': this.get_int_css(this.select, 'top'),
      'left': this.get_int_css(this.select, 'left'),
      'width': this.select.width(),
      'height': this.select.height()
    };
  },
  'fun_set_wh_valid': function() {
    return {
      'w': [this.resize_size + 2 * this.select_border_w,
        this.container.width() - this.get_int_css(this.select, 'left') - 2 * this.select_border_w],
      'h': [this.resize_size + 2 * this.select_border_w,
        this.container.height() - this.get_int_css(this.select, 'top') - 2 * this.select_border_w]
    };
  },
  'bind_set_wh': function() {
    var th = this;
    this.fieldset.find('input[type="button"]').click(function() {
      var v = th.fun_set_wh_valid();
      var width = th.valid_num(th.field_width.val(), v['w'][0], v['w'][1]);
      var height = th.valid_num(th.field_height.val(), v['h'][0], v['h'][1]);
      th.select.width(width).height(height);
      th.resize.css('left', width - th.resize_size + 'px').css('top', height - th.resize_size + 'px');
      th.field_width.val(width);
      th.field_height.val(height);
    });
  },
  'fill_field': function() {
    this.field_width.val(parseInt(this.select.width()), 10);
    this.field_height.val(parseInt(this.select.height(), 10));
  },
  'result': function() {
    var t = this.get_int_css(this.select, 'top');
    var l = this.get_int_css(this.select, 'left');
    return {
      'src': this.src,
      'top': t,
      'left': l,
      'width': this.select.width(),
      'height': this.select.height()
    };
  }
};
