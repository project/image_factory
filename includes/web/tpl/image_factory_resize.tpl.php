<?php
/**
 * @file
 * The image resize templete.
 */
?>
<!-- drage image  area -->
<div id = "drage_image_contianer">
  <div id = "drage_image_opacity">

  </div>
  <div id = "drage_image_select" style="">
    <div id = "drage_image_resize" ></div>
  </div>
</div>
<div style="clear:both"></div>
<fieldset id="drage_image_fieldset">
  <table>
    <tr>
      <td><label>Drag area's width</label></td>
      <td><input type="text" id="drage_image_width"></td>
    </tr>
    <tr>
      <td><label>Drag area's height</label></td>
      <td> <input type="text" id="drage_image_height"></td>
    </tr>
    <tr>
      <td><label>Apply width and height to drag area</label></td>
      <td><input type="button" id="drage_image_button" value="Apply"></td>
    </tr>
  </table>
</fieldset>
<!-- drage image area end -->
