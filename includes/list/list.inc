<?php

/**
 * @file
 * File usage.
 */

/**
 * Show image list view.
 * 
 * @return string
 *   View of image list.
 */
function image_factory_image_list() {
  $a_html = '<div class="image_factory_goto_upload"><ul>
      <li>' . l(t('Upload fonts'), 'admin/image_factory/fonts/upload', array('absolute' => TRUE)) . '</li>
      <li>' . l(t('Manage fonts'), 'admin/image_factory/fonts/list', array('absolute' => TRUE)) . '</li>
      <li>' . l(t('Upload image'), 'admin/image_factory/upload', array('absolute' => TRUE)) . '</li>
		</ul></div>';
  $count = db_query("SELECT COUNT(*) FROM {file_managed} fm where  fm.filemime like :pattern", array(':pattern' => 'image/%'))
      ->fetchField();
  $page_num = pager_default_initialize($count, 10);
  $pager_html = theme('pager', array('quantity' => 8));

  $query = db_query_range("select * from {file_managed} fm where fm.filemime like :pattern order by fm.timestamp desc ", $page_num * 10, 10, array(':pattern' => 'image/%'));
  $result = $query->fetchAllAssoc('fid', PDO::FETCH_ASSOC);

  $file_html = theme_image_factory_file_formatter_table(array('items' => $result));
  return $a_html . $pager_html . $file_html . $pager_html;
}
